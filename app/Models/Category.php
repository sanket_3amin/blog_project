<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $table = 'categories';
    protected $primaryKey = 'id';
    protected $fillable = ['catgory_name'];
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    protected $dates = ['deleted_at'];
}
