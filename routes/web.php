<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/posts', 'App\Http\Controllers\PostController@index')->name('posts.index');
Route::get('/posts/add', 'App\Http\Controllers\PostController@create')->name('posts.create');
Route::post('/posts/create', 'App\Http\Controllers\PostController@store')->name('posts.store');
Route::get('/posts/show/{id}', 'App\Http\Controllers\PostController@show')->name('posts.show');
Route::get('/posts/delete/{id}', 'App\Http\Controllers\PostController@destroy')->name('posts.destroy');
Route::delete('/posts/delete/{id}', 'App\Http\Controllers\PostController@destroy')->name('posts.destroy');
Route::get('/posts/edit/{id}', 'App\Http\Controllers\PostController@edit')->name('posts.edit');
Route::post('/posts/update/{id}', 'App\Http\Controllers\PostController@update')->name('posts.update');
//Route::resource('posts', 'PostController');
